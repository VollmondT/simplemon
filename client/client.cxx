#include <iostream>
#include <string>
#include <getopt.h>
#include "ClientApplication.h"

using namespace std;


/**
 * @brief Отобразить хелп на экране
 */
static void show_help() {
	cerr << "client usage:" << endl
		<< "\tclient -t <api token> <doted-host> <port number>" << endl;
}


int main( int argc, char **argv ) {
	string token;
	string host;
	uint16_t port;

	int opt;
	while( (opt = getopt(argc, argv, "t:h") ) != -1 ) {
		switch( opt ) {
		case 't':
			token = optarg;
			break;

		case 'h':
			show_help();
			return 0;

		default:
			show_help();
			return 1;
		}
	}

	if( optind < argc ) {
		host = argv[optind++];
	} else {
		cerr << "parameter \"host\" not given!" << endl;
		show_help();
		return 1;
	}

	if( optind < argc ) {
		port = atoi( argv[optind] );
	} else {
		cerr << "parameter \"port\" not given!" << endl;
		show_help();
		return 1;
	}

	if( token.empty() ) {
		cerr << "api token is empty!" << endl;
		show_help();
		return 1;
	}

	try {
		return ClientApplication(token, host, port).run();
	} catch( std::exception &ex ) {
		cerr << "exception catched: " << ex.what() << endl;
	} catch(...) {
		cerr << "unhandled exception" << endl;
	}

	return 1;
}