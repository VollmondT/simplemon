#ifndef CLIENTAPPLICATION_H
#define CLIENTAPPLICATION_H

#include <string>
#include <stdint.h>
#include <BaseApplication.h>

class ClientHandler;

/**
 * @brief Клиентское приложение
 * 
 * Реализует логику клиента
 */
class ClientApplication: public BaseApplication {
public:
	/**
	 * @brief Конструктор
	 * 
	 * @param token - строка авторизации
	 * @param host - хост
	 * @param port - порт
	 */
	ClientApplication( const std::string &token, const std::string &host, uint16_t port );
};


#endif // CLIENTAPPLICATION_H
