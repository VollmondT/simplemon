#include "ClientHandler.h"
#include <BaseApplication.h>
#include <LoadAvg.h>
#include <TcpStat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <stdexcept>
#include <cerrno>
#include <cstring>
#include <iostream>
#include <sstream>

using namespace std;


ClientHandler::ClientHandler ( const std::string& token, const std::string& host, uint16_t port ) {
	m_fd = socket( AF_INET, SOCK_STREAM, 0 );
	if( m_fd == -1 ) {
		throw runtime_error( string("ClientHandler::ClientHandler: socket - ") +
			strerror(errno));
	}

	sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = inet_addr( host.c_str() );
	if( connect( m_fd, reinterpret_cast<sockaddr*>(&addr), sizeof(addr) ) ) {
		throw runtime_error( string("ClientHandler::ClientHandler: connect - ") +
			strerror(errno));
	}

	ostringstream os;
	os << "auth: " << token << '\n';
	if( !sendAll(os.str()) ) {
		throw runtime_error( "ClientHandler::ClientHandler: can't send auth data" );
	}

	m_collect_timer = App()->m_timers.addTimer(1000, [=]( TimerPool::id_t ) {
		m_ifaces.parseProc();
	});

	m_send_timer =  App()->m_timers.addTimer(10000, [=]( TimerPool::id_t ) {
		sendData();
	});
}


ClientHandler::~ClientHandler() {
	App()->m_timers.deleteTimer(m_collect_timer);
	App()->m_timers.deleteTimer(m_send_timer);
}


bool ClientHandler::onData() {
	// активность от сервера у нас не заложена
	cout << "closing connection..." << endl;
	BaseApplication::getInstance()->leaveLoop(1);
	return false;
}


void ClientHandler::sendData() {
	ostringstream os;

	LoadAvg avg;
	avg.getNow().dumpPacket(os);
	avg.dumpPacket(cout);

	TcpStat tcp;
	tcp.parseProc().dumpPacket(os);
	tcp.dumpPacket(cout);

	m_ifaces.dumpPacket(os);
	m_ifaces.dumpPacket(cout);

	if( !sendAll(os.str()) ) {
		cerr << "can't send data" << endl;
		BaseApplication::getInstance()->leaveLoop(1);
	}
}


bool ClientHandler::sendAll ( const string& line ) {
	const char* ptr = line.c_str();
	int to_send = line.length();
	while( to_send ) {
		int sended = send( m_fd, ptr, to_send, MSG_NOSIGNAL );
		if( sended == -1 ) {
			cerr << "send error: " << strerror(errno) << endl;
			return false;
		}

		to_send -= sended;
		ptr += sended;
	}
	return true;
}
