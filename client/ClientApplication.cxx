#include "ClientApplication.h"
#include "ClientHandler.h"

ClientApplication::ClientApplication ( const std::string& token, const std::string& host, uint16_t port ) {
	auto handler = new ClientHandler(token, host, port);
	handler->sendData();
	addEventSource(handler);
}
