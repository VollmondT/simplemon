#ifndef CLIENTHANDLER_H
#define CLIENTHANDLER_H

#include <FdEventSource.h>
#include <string>
#include <stdint.h>
#include "TimerPool.h"
#include <IfaceStat.h>

/**
 * @brief Обработчик событий на клиентском дескрипторе
 */
class ClientHandler: public FdEventSource {
public:
	/**
	 * @brief Конструктор
	 * 
	 * @param token - строка авторизации
	 * @param host - хост
	 * @param port - порт
	 */
	ClientHandler( const std::string &token, const std::string &host, uint16_t port );
	
	
	/**
	 * @brief Деструктор
	 * 
	 * Удаляет таймеры
	 */
	virtual ~ClientHandler();


	/**
	 * @overload
	 */
	virtual bool onData();


	/**
	 * @brief Послать собранные данные
	 */
	void sendData();

private:
	/**
	 * @brief Отправить полную порцию данных
	 * 
	 * @param line - данные для отправки
	 * @return true - успех
	 */
	bool sendAll(const std::string &line);


	/**
	 * @brief Идентификатор таймера для сбора информации
	 */
	TimerPool::id_t m_collect_timer;


	/**
	 * @brief Идентификатор таймера на отправку информации
	 */
	TimerPool::id_t m_send_timer;


	/**
	 * @brief Калькулятор network bandwidth
	 * 
	 * Ему раз в секунду нужно дёргать IfaceStatCalculator::parseProc()
	 */
	IfaceStatCalculator m_ifaces;
};

#endif // CLIENTHANDLER_H
