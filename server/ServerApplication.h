#ifndef SERVERAPPLICATION_H
#define SERVERAPPLICATION_H

#include <BaseApplication.h>
#include <string>
#include <stdint.h>

class ServerSocketAcceptor;

/**
 * @brief Класс, реализующий логику серверного приложения
 */
class ServerApplication: public BaseApplication {
public:
	/**
	 * @brief Конструктор
	 * @param port - порт, на котором слушать соединения
	 * @param token - токен для авторизации
	 */
	ServerApplication( uint16_t port, const std::string &token );


	/**
	 * @brief Получить авторизационный токен
	 * @return m_token
	 */
	const std::string& getToken() {
		return m_token;
	}


	/**
	 * @brief Распечатать значения мониторов клиентов
	 * @return void
	 */
	void dumpClients();

private:
	/**
	 * @brief Токен для авторизации клиентов
	 */
	std::string m_token;


	/**
	 * @brief Серверный сокет
	 */
	FdEventSource* m_server;
};

#endif // SERVERAPPLICATION_H
