#ifndef CLIENTHANDLER_H
#define CLIENTHANDLER_H

#include <FdEventSource.h>
#include <LoadAvg.h>
#include <TcpStat.h>
#include <IfaceStat.h>
#include <ostream>
#include <string>
#include <memory>
#include <vector>


class ServerState;

/**
 * @brief Обработчик активности от клиента
 */
class ServerHandler: public FdEventSource {
public:
	/**
	 * @brief Конструктор
	 * @param fd - файловый дескриптор (сокет клиента)
	 * @param ident - идентификатор клиента
	 */
	ServerHandler( int fd, const std::string &name );


	/**
	 * @brief Установить новое состояние клиента
	 * @param newstate - состояние
	 */
	void setState( const std::shared_ptr<ServerState> &newstate );


	/**
	 * @brief Получить имя клиента
	 * @return m_name
	 */
	const std::string& getName() {
		return m_name;
	}


	/**
	 * @brief Вывести в поток текущее состояние мониторов
	 * @param stream - поток для вывода 
	 */
	void dumpMonitors( std::ostream &stream );


	/**
	 * @brief Получить LoadAvg
	 * @return m_avg
	 */
	LoadAvg& getLoadAvg() {
		return m_avg;
	}


	/**
	 * @brief Получить статистику TCP
	 * @return m_tcp
	 */
	TcpStat& getTcpStat() {
		return m_tcp;
	}


	/**
	 * @brief Получить статистику по интерфейсам
	 * @return m_ifaces
	 */
	IfacesStatList& getIfacesStat() {
		return m_ifaces;
	}

protected:
	/**
	 * @brief Максимальная длина строки протокола
	 */
	static const unsigned m_max_line_size;


	/**
	 * @brief Результат чтения строки протокола
	 */
	enum ReadState {
		/**
		 * @brief Превышение длины строки
		 */
		rsTooLarge,

		/**
		 * @brief Частичное чтение
		 */
		rsPart,

		/**
		 * @brief Строка прочитана
		 */
		rsReady
	};


	/**
	 * @overload
	 */
	virtual bool onData();


	/**
	 * @brief Прочитать порцию данных из канала в буфер
	 * @return false - если конец файла или ошибка чтения
	 */
	bool pushBuf();


	/**
	 * @brief Прочитать очередную строку протокола
	 * @return ReadState
	 */
	ReadState readLine(std::string &line);


	/**
	 * @brief Идентификатор клиента
	 */
	std::string m_name;


	/**
	 * @brief Указатель на текущее состояние
	 */
	std::shared_ptr<ServerState> m_state;


	/**
	 * @brief Указатель на следующее состояние
	 */
	std::shared_ptr<ServerState> m_next_state;


	/**
	 * @brief Приёмный буфер
	 */
	std::vector<char> m_buf;


	/**
	 * @brief Кешированное значение load avg
	 */
	LoadAvg m_avg;


	/**
	 * @brief Кешированное значение статистики tcp соединений
	 */
	TcpStat m_tcp;


	/**
	 * @brief Кешированное значение статистики по интерфейсам
	 */
	IfacesStatList m_ifaces;
};

#endif // CLIENTHANDLER_H
