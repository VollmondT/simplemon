#include <iostream>
#include "ServerApplication.h"
#include "ServerSocketAcceptor.h"
#include "ServerHandler.h"

using namespace std;


ServerApplication::ServerApplication( uint16_t port, const std::string &token )
: m_token(token), m_server( new ServerSocketAcceptor(port) ) {
	addEventSource(m_server);

	m_timers.addTimer(1000, [this](TimerPool::id_t) {
		dumpClients();
	});
}


void ServerApplication::dumpClients() {
	// очистка экрана
	cout << '\x1b' << 'c';
	cout.flush();

	cout << "Clients connected: " << m_sources.size() -1 << endl << endl;
	for( auto &x: m_sources ) {
		if( x.get() == m_server ) {
			continue;
		}
		// все остальные - клиенты
		static_cast<ServerHandler*>(x.get())->dumpMonitors(cout);
	}
}
