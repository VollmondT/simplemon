#ifndef SERVERSOCKETACCEPTOR_H
#define SERVERSOCKETACCEPTOR_H

#include <FdEventSource.h>
#include <stdint.h>


/**
 * @brief Слушающий сокет
 * 
 * В конструкторе инициализирует сокет, в onReadEvent() - принимает соединения
 */
class ServerSocketAcceptor: public FdEventSource {
public:
	/**
	 * @brief Конструктор
	 * 
	 * Создает, биндит и начинает слушать на сокете
	 * @param port - порт
	 */
	ServerSocketAcceptor( uint16_t port );


	/**
	 * @overload
	 */
	virtual bool onData();
};

#endif // SERVERSOCKETACCEPTOR_H
