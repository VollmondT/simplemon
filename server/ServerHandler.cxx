#include <unistd.h>
#include <iostream>
#include <cstring>
#include <cerrno>
#include <iomanip>
#include <algorithm>
#include <BaseApplication.h>
#include "ServerHandler.h"
#include "ServerApplication.h"

using namespace std;


/**
 * @brief Базовый класс - состояние клиента
 */
struct ServerState {

	/**
	 * @brief Конструктор с заданием контекста
	 * @param client - контекст состояния
	 */
	ServerState( ServerHandler& client ): m_ctx(client) {
	}


	/**
	 * @brief Виртуальный деструктор
	 */
	virtual ~ServerState() {};


	/**
	 * @brief Контекст данного состояния
	 */
	ServerHandler& m_ctx;


	/**
	 * @brief Обработка строки протокола
	 * 
	 * @param header - строка протокола
	 * @param data - строка протокола
	 * @return false - если клиент нарушает протокол
	 */
	virtual bool handleLine(string &header, string &data) = 0;
};


/**
 * @brief Состояние клиента после соединения
 * 
 * В этом состоянии разрешено только получить строку авторизации, прочие пакеты нелигитимны и
 * приведут к разрыву соединения сервером.
 */
struct NotAuthorizedState: ServerState {
	/**
	 * @brief Конструктор
	 * @param client - контекст состояния
	 */
	NotAuthorizedState ( ServerHandler& client ): ServerState(client) {
	}


	/**
	 * @overload
	 */
	virtual bool handleLine ( string &header, string &data );
};


/**
 * @brief Обычное состояние клиента
 * 
 * В этом состоянии разрешены все пакеты, кроме строки авторизации
 */
struct ReadyState: ServerState {
	/**
	 * @brief Конструктор
	 * @param client - контекст состояния
	 */
	ReadyState ( ServerHandler& client ): ServerState(client) {
	}


	/**
	 * @overload
	 */
	virtual bool handleLine ( string& header, string &data );
};


const unsigned ServerHandler::m_max_line_size = 1024;


ServerHandler::ServerHandler( int fd, const string& ident ):
	FdEventSource(fd),
	m_name(ident),
	m_next_state( make_shared<NotAuthorizedState>(*this) ) {
	cerr << "New client connected: " << m_name << endl;
}


/**
 * @brief Разбить строку на заголовок и данные
 * @param line - входная строка
 * @param header - заголовок (выход)
 * @param data - данные (выход)
 * @return false - если строка не может быть разбита
 */
static bool getTokens( const string& line, string &header, string &data ) {
	auto pos = line.find_first_of( ':' );
	if( pos == line.npos ) {
		return false;
	}

	header = line.substr(0, pos);

	if( ++pos == line.length() ) {
		return false;
	}
	
	pos = line.find_first_not_of( ' ', pos );
	if( pos == line.npos ) {
		return false;
	}

	data = line.substr(pos);
	return true;
}


bool ServerHandler::onData() {
	if( !pushBuf()  ) {
		cerr << m_name << " - closing connection" << endl;
		return false;
	}

	while( true ) {
		string line;
		switch( readLine(line) ) {
		case rsPart:
			return true;

		case rsTooLarge:
			cerr << m_name << " - packet too large!" << endl;
			return false;

		default:
			break;
		}

		// переход состояния
		if( m_next_state ) {
			m_state = m_next_state;
			m_next_state.reset();
		}

		string header, data;
		if( !getTokens(line, header, data) ) {
			cerr << m_name << " - protocol mismatch!" << endl;
			return false;
		}

		// строка получена
		if( !m_state->handleLine( header, data ) ) {
			cerr << m_name << " - closing connection" << endl;
			return false;
		}
	}

	return true;
}


bool ServerHandler::pushBuf() {
	char buf[ m_max_line_size ];
	int readen = read( m_fd, buf, sizeof(buf) - m_buf.size() );
	if( readen == -1 ) {
		cerr << m_name << " - read(): " << strerror(errno) <<  endl;
		return false;
	} else if( readen == 0 ) {
		return false;
	}

	m_buf.reserve(readen);
	for( int i = 0; i < readen; ++i ) {
		m_buf.push_back(buf[i]);
	}

	return true;
}


ServerHandler::ReadState ServerHandler::readLine ( string& line ) {
	auto pos = find( m_buf.begin(), m_buf.end(), '\n' );

	if( pos == m_buf.end() ) {
		if( m_buf.size() == m_max_line_size ) {
			return rsTooLarge;
		}
		return rsPart;
	}

	line.insert( line.end(), m_buf.begin(), pos );
	m_buf.erase( m_buf.begin(), pos+1 );
	return rsReady;
}


void ServerHandler::setState ( const shared_ptr< ServerState >& newstate ) {
	// когда будет следующая итерация в onData(), то текущее состояние заменится на newstate
	m_next_state = newstate;
}


void ServerHandler::dumpMonitors ( ostream& stream ) {
	stream << m_name << endl;
	stream << "\tLoad avg:";
	for( auto &x: m_avg.m_samples ) {
		stream << ' ' << setprecision(2) << x;
	}
	stream 	<< "\n\tTcp:   listen - " << m_tcp.m_listen
		<< "   established - " << m_tcp.m_established
		<< "   closing - " << m_tcp.m_closing;
	stream	<< "\n\tNetwork bandwidth in/out (bytes/sec):\n";
	for( auto &x: m_ifaces.m_stat ) {
		stream << '\t' << '\t' << x.first
			<< '\t' << x.second.m_rx << '\t' << x.second.m_tx << endl;
	}
	stream << endl;
}


bool NotAuthorizedState::handleLine ( string& header, string& data )  {
	string token( static_cast<ServerApplication*>(BaseApplication::getInstance())->getToken() ); 

	if( (header != "auth") || (data != token) ) {
		cerr << m_ctx.getName() << " - auth failed!" << endl;
		return false;
	}

	m_ctx.setState( make_shared<ReadyState>(m_ctx) );
	return true;
}


bool ReadyState::handleLine ( string& header, string& data ) {
	if( header == "load avg" ) {
		return m_ctx.getLoadAvg().parseString(data);
	} else if( header == "tcp" ) {
		return m_ctx.getTcpStat().parseString(data);
	} else if( header == "ifaces" ) {
		return m_ctx.getIfacesStat().parseString(data);
	}
	return true;
}
