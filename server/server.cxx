#include <iostream>
#include <string>
#include <getopt.h>
#include "ServerApplication.h"


using namespace std;


/**
 * @brief Отобразить хелп на экране
 */
static void show_help() {
	cerr << "server usage:" << endl
		<< "\tserver -p <port number> -t <auth token>" << endl
		<< "\t-p - listen port" << endl
		<< "\t-t - auth token" << endl;
}


/**
 * @brief Точка входа
 * 
 * @param argc 
 * @param argv
 * @return int
 */
int main( int argc, char **argv ) {
	int opt;
	uint16_t port = 0; // listen port
	std::string token; // auth token

	// скучаю по boost::programm_options
	while( (opt = getopt(argc, argv, "p:t:h")) != -1 ) {
		switch(opt) {
		case 'p':
			port = atoi(optarg);
			break;

		case 't':
			token = optarg;
			break;

		case 'h':
			show_help();
			return 0;

		default:
			show_help();
			return 1;
		}
	}

	// порт и токен не введены корректно
	if( !port || token.empty() ) {
		cerr << "both parameters port and auth token must be entered!" << endl << endl;
		show_help();
		return 1;
	}

	try {
		return ServerApplication(port, token).run();
	} catch( std::exception &ex ) {
		cerr << "exception catched: " << ex.what() << endl;
	} catch( ... ) {
		cerr << "unhandled exception!" << endl;
	}

	return 1;
}
