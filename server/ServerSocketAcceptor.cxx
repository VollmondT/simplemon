#include "ServerSocketAcceptor.h"
#include "ServerHandler.h"
#include <BaseApplication.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <cstring>
#include <cerrno>
#include <stdexcept>
#include <iostream>
#include <sstream>


using namespace std;


ServerSocketAcceptor::ServerSocketAcceptor ( uint16_t port ) {
	m_fd = socket( AF_INET, SOCK_STREAM, 0 );
	if( m_fd == -1 ) {
		throw runtime_error( 
			string("ServerSocketAcceptor::ServerSocketAcceptor() socket: ") + 
			strerror(errno)
		);
	}

	struct sockaddr_in addr;
	memset( &addr, 0, sizeof( addr ) );
	addr.sin_family = AF_INET;
	addr.sin_port = htons( port );
	addr.sin_addr.s_addr = INADDR_ANY;

	if( bind( m_fd, reinterpret_cast<struct sockaddr*>(&addr), sizeof(addr) ) ) {
		// базовый класс закроет открытый дескриптор, кидать не страшно
		throw runtime_error( 
			string("ServerSocketAcceptor::ServerSocketAcceptor() bind: ") + 
			strerror(errno)
		);
	}

	if( listen( m_fd, 10 ) ) {
		throw runtime_error( 
			string("ServerSocketAcceptor::ServerSocketAcceptor() listen: ") + 
			strerror(errno)
		);
	}
}


bool ServerSocketAcceptor::onData() {
	sockaddr_in addr;
	socklen_t len = sizeof(addr);
	int socket = accept( m_fd, reinterpret_cast<struct sockaddr*>(&addr), &len );
	if( socket == -1 ) {
		cerr << "can't accept connection " << strerror(errno) << endl;
		// в любом случае, продолжаем работу
		return true;
	}

	// сгенерируем имя клиента
	ostringstream os;
	int i;
	for( i = 0; i < 24; i+=8 ) {
		os << ((addr.sin_addr.s_addr >> i) & 0xff) << '.';
	}
	os << ((addr.sin_addr.s_addr >> i) & 0xff) << ':' << ntohs(addr.sin_port);

	BaseApplication::getInstance()->addEventSource( new ServerHandler(socket, os.str()) );
	return true;
}

