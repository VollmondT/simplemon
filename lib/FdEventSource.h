#ifndef FDEVENTSOURCE_H
#define FDEVENTSOURCE_H

class BaseApplication;

/**
 * @brief Источник события на дескрипторе
 */
class FdEventSource {
public:
	friend class BaseApplication;


	/**
	 * @brief Конструктор
	 * @param fd - файловый дескриптор для ожидания на нем события
	 * @param own - разрешение на владение дескриптором
	 */
	FdEventSource( int fd = -1, bool own = true );


	/**
	 * @brief Виртуальный деструктор
	 */
	virtual ~FdEventSource();

protected:
	/**
	 * @brief Обработчик активности на дескрипторе
	 * @return false - если данный обработчик должен быть удален из пула
	 */
	virtual bool onData() = 0;


	/**
	 * @brief Закрыть дескриптор
	 * 
	 * В случае неудаче бросает runtime_error с расшифровкой кода
	 */
	void close();


	/**
	 * @brief Файловый дескриптор
	 */
	int m_fd;


	/**
	 * @brief Является ли данный экземпляр владельцем дескриптора?
	 */
	bool m_own;
};

#endif // FDEVENTSOURCE_H
