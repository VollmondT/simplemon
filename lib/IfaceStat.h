#ifndef IFACESTAT_H
#define IFACESTAT_H

#include <vector>
#include <string>
#include <map>
#include <stdint.h>

/**
 * @brief Статистика отедельного интерфейса
 */
struct IfaceStat {
	uint64_t m_tx;
	uint64_t m_rx;
	IfaceStat(): m_tx(0), m_rx(0) {}
};


/**
 * @brief Список интерфейсов
 */
struct IfaceStatCalculator {

	/**
	 * @brief Конструктор
	 * 
	 * Делает первый замер
	 */
	IfaceStatCalculator();


	/**
	 * @brief Два замера для ежесекундного расчета
	 */
	struct Deltas {
		/**
		 * @brief Старый замер
		 */
		IfaceStat old;


		/**
		 * @brief Новый замер
		 */
		IfaceStat current;
	};


	/**
	 * @brief Статистика по интерфейсам
	 */
	std::map< std::string, Deltas> m_stat;


	/**
	 * @brief Перечитать /proc/net/dev
	 * 
	 * Дергать строго раз в секунду
	 */
	bool parseProc();


	/**
	 * @brief Сдампить пакет для канала
	 * @param stream - поток для сохранения дампа
	 */
	void dumpPacket( std::ostream &stream ) const;
};


/**
 * @brief Список мгновенных значений для клиентской стороны
 */
struct IfacesStatList {
	/**
	 * @brief Статистика по интерфейсам
	 */
	std::map< std::string, IfaceStat> m_stat;


	/**
	 * @brief Распарсить строку протокола
	 * 
	 * @param s - поле data строки протокола
	 * @return true - успех
	 */
	bool parseString(const std::string &s);
};

#endif // IFACESTAT_H
