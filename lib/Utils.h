#ifndef UTILS_H
#define UTILS_H

#include <string>
#include <vector>
#include <cctype>
#include <functional>

/**
 * @brief Разбить строку на токены
 * 
 * @param str - исходная строка
 * @param predicate - условие разделителя
 * @return std::vector< std::string >
 */
std::vector<std::string> getTokens( const std::string &str,
	const std::function<bool(char)> &predicate = isspace );


/**
 * @brief Обрезать witespace с левого конца строки
 * 
 * @param str - исходная строка
 * @return std::string
 */
std::string ltrimCopy( const std::string &str );

#endif // UTILS_H
