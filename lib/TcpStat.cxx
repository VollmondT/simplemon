#include <netinet/tcp.h>

#include <fstream>
#include <sstream>
#include <string>
#include <algorithm>
#include <stdexcept>
#include "TcpStat.h"
#include "Utils.h"

using namespace std;


TcpStat& TcpStat::parseProc() {
	ifstream ifs("/proc/net/tcp");
	if( !ifs.is_open() ) {
		throw runtime_error("can't open /proc/net/tcp");
	}

	string line;

	getline(ifs, line); // заголовок

	while( getline(ifs, line) ) {
		int flag = strtol( getTokens(line).at(3).c_str(), nullptr, 16 );
		switch( flag ) {
		case TCP_LISTEN:
			++m_listen;
			break;

		case TCP_ESTABLISHED:
			++m_established;
			break;

		case TCP_CLOSING:
			++m_closing;
		}
	}

	return *this;
}


void TcpStat::dumpPacket ( ostream& stream ) const {
	stream << "tcp:"
		<< ' ' << m_listen
		<< ' ' << m_established
		<< ' ' << m_closing << '\n';
}


bool TcpStat::parseString ( const string& s ) {
	istringstream is(s);
	return (is >> m_listen >> m_established >> m_closing);
}
