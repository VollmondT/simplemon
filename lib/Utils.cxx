#include <algorithm>
#include "Utils.h"

using namespace std;


vector< string > getTokens ( const string& str, const function< bool (char c)>& predicate ) {
	vector<string> ret;

	auto start_pos = str.begin();

	while( true ) {
		start_pos = find_if_not(start_pos, str.end(), predicate);

		if( start_pos == str.end() ) {
			return ret;
		}

		auto stop_pos = find_if(start_pos, str.end(), predicate );
		ret.emplace_back( string(start_pos, stop_pos) );
		start_pos = stop_pos;
	}

	return ret;
}


string ltrimCopy ( const string& str ) {
	auto start = find_if_not( str.begin(), str.end(), [] (char c) -> bool {
		return isspace(c);
	});
	return string( start, str.end() );
}
