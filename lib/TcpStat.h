#ifndef TCPSTAT_H
#define TCPSTAT_H

#include <ostream>

/**
 * @brief Статистика соединений TCP
 */
struct TcpStat {
	/**
	 * @brief Количество слушающих сокетов
	 */
	unsigned m_listen;


	/**
	 * @brief Количество установленных соединений
	 */
	unsigned m_established;


	/**
	 * @brief Количество соединений в состоянии TCP_CLOSING
	 */
	unsigned m_closing;


	/**
	 * @brief Конструктор по умолчанию
	 */
	TcpStat(): m_listen(0), m_established(0), m_closing(0) {}


	/**
	 * @brief Прочитать данные из /proc/net/tcp
	 * 
	 * Может кинуть runtime_error, если файл в /proc отсутсвует
	 * @return *this
	 */
	TcpStat& parseProc();


	/**
	 * @brief Сдампить пакет для канала
	 * @param stream - поток для сохранения дампа
	 */
	void dumpPacket( std::ostream &stream ) const;


	/**
	 * @brief Парсинг строки данных протокола
	 * @param s - поле data строки протокола
	 * @return true - успех
	 */
	bool parseString( const std::string &s );
};

#endif // TCPSTAT_H
