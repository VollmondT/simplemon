#include "FdEventSource.h"
#include <unistd.h>
#include <stdexcept>
#include <cstring>
#include <cerrno>


FdEventSource::FdEventSource ( int fd, bool own ): m_fd(fd), m_own(own) {
	
}


void FdEventSource::close() {
	if( m_own && (m_fd != -1)  ) {
		if( ::close(m_fd) ) {
			throw std::runtime_error(std::string("FdEventSource::close() : ")
				+ strerror(errno) );
		}
		m_fd = -1;
	}
}


FdEventSource::~FdEventSource() {
	try {
		close();
	} catch( std::exception& )  {
		// серьезная ошибка, нужно хотябы залоггировать её с высоким приоритетом
	}
}
