#include "LoadAvg.h"
#include <cstdlib>
#include <sstream>
#include <iomanip>


using namespace std;


LoadAvg::LoadAvg() {
	for( auto &x: m_samples ) {
		x = 0.0;
	}
}


LoadAvg& LoadAvg::getNow() {
	getloadavg(&m_samples[0], 3);
	return *this;
}


bool LoadAvg::parseString ( const std::string& str ) {
	istringstream is(str);
	for( auto &x: m_samples ) {
		if( ! (is >> x) ) {
			return false;
		}
	}
	return true;
}


void LoadAvg::dumpPacket ( ostream& stream ) const {
	stream << "load avg:";
	for( auto &x: m_samples) {
		stream << ' ' << std::fixed << std::setprecision( 2 ) << x;
	}
	stream << '\n';
}
