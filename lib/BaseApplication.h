#ifndef BASEAPPLICATION_H
#define BASEAPPLICATION_H

#include <memory>
#include <list>
#include "TimerPool.h"

class FdEventSource;

#define App() BaseApplication::getInstance()

/**
 * @brief Приложение (базовый класс)
 * 
 * Реализует основной цикл и перехват сигналов. Данный класс не может быть сконструирован дважды.
 */
class BaseApplication {
public:
	/**
	 * @brief Конструктор
	 * 
	 * Инициализирует m_instance, устанавливает обработчики сигналов, не конструирует дважды
	 * (бросает исключение)
	 */
	BaseApplication();


	/**
	 * @brief Копирование запрещено
	 */
	BaseApplication( const BaseApplication& ) = delete;


	/**
	 * @brief Виртуальный деструктор
	 */
	virtual ~BaseApplication();


	/**
	 * @brief Главный цикл приложения
	 * @return код возврата для main()
	 */
	int run();


	/**
	 * @brief Мягко завершить главный цикл
	 * @param code - код завершения
	 */
	void leaveLoop( int code ) {
		m_code = code;
		m_leaveLoop = true;
	}


	/**
	 * @brief Указатель на глобальные экземпляр
	 * @return m_instance
	 */
	static BaseApplication *getInstance() {
		return m_instance;
	}


	/**
	 * @brief Добавить дескриптор
	 * @param source - обработчик событий на дескрипторе
	 */
	void addEventSource( FdEventSource *source );


	/**
	 * @brief Обработчик UNIX сигнала
	 * @param sig - код сигнал
	 */
	virtual void onSignal( int sig );


	/**
	 * @brief Пул таймеров, обрабатываемый в главном цикле
	 */
	TimerPool m_timers;

protected:
	/**
	 * @brief Признак выходя из главного цикла
	 */
	bool m_leaveLoop;


	/**
	 * @brief Код возврата run()
	 */
	int m_code;


	/**
	 * @brief Указатель для синглтона
	 */
	static BaseApplication *m_instance;


	/**
	 * @brief Список дескрипторов
	 */
	std::list< std::unique_ptr<FdEventSource> > m_sources;
};

#endif // BASEAPPLICATION_H
