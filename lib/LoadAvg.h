#ifndef LOADAVG_H
#define LOADAVG_H

#include <ostream>

/**
 * @brief Структура для хранения LoadAvg
 */
struct LoadAvg {
	/**
	 * @brief Конструктор по умолчанию
	 */
	LoadAvg();


	/**
	 * @brief Получить load avg на текущей машине
	 */
	LoadAvg& getNow();


	/**
	 * @brief Сдампить для посылки по каналу связи
	 * @param stream - поток для сохранения
	 */
	void dumpPacket(std::ostream &stream) const;


	/**
	 * @brief Распарсить строку data протокола
	 * @param str - строка
	 * @return true - удачно
	 */
	bool parseString( const std::string &str );


	/**
	 * @brief Семплы для getloadavg
	 */
	double m_samples[3];
};

#endif // LOADAVG_H
