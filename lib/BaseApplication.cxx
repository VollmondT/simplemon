#include <sys/select.h>
#include <iostream>
#include <stdexcept>

#include "FdEventSource.h"
#include "BaseApplication.h"

using namespace std;

BaseApplication* BaseApplication::m_instance = nullptr;

BaseApplication::BaseApplication(): m_leaveLoop(false), m_code(0) {
	if( m_instance ) {
		throw runtime_error("only one instance of BaseApplication is allowed!");
	}
	m_instance = this;
	// TODO установить обработчик сигнала и дергать в нем 
	// BaseApplicationon::getInstance()->onSignal()
}


BaseApplication::~BaseApplication() {
	// на случай если получим сигнал, пока складываемся
	m_instance = nullptr;
}


int BaseApplication::run() {
	int errcnt = 0;

	while( !m_leaveLoop ) {
		fd_set fds;
		FD_ZERO(&fds);

		int max_fd = -1;
		for( auto &x: m_sources ) {
			FD_SET(x->m_fd, &fds);
			if( max_fd < x->m_fd ) {
				max_fd = x->m_fd;
			}
		}
		
		struct timeval tv;
		// если таймеров нет, то ждем вечно на дескрипторах
		int ret = select( max_fd + 1, &fds, nullptr, nullptr, 
			m_timers.getNextShootTimeInterval(tv) ? &tv : nullptr );
		if( ret == -1 ) {
			if( ++errcnt > 10 ) {
				// мало ли что непредвиденно заглючит, на время отладки это точно
				// пригодится
				cerr << "too many errors in last time!" << endl;
				leaveLoop(1);
			}
			continue;
		}

		if( ret > 0 ) {
			// тут идёт проверка event'ов на дескрипторах
			for( auto it = m_sources.begin(); it != m_sources.end(); ++it ) {
				if( FD_ISSET( (*it)->m_fd, &fds) ) {
					if( !(*it)->onData() ) {
						// удаляем из пула
						m_sources.erase(it--);
					}

					if( --ret ) {
						continue;
					}
					// обработали всех
					break;
				}
			}
		}

		m_timers.check(); // обработка callback'ов таймеров здесь
	}

	cout << "exiting with code " << m_code << endl;
	return m_code;
}


void BaseApplication::addEventSource ( FdEventSource* source ) {
	m_sources.emplace_back(source);
}


void BaseApplication::onSignal( int ) {
	
}
