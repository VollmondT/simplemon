#include <fstream>
#include <stdexcept>
#include <algorithm>
#include <sstream>
#include "IfaceStat.h"
#include "Utils.h"

using namespace std;

IfaceStatCalculator::IfaceStatCalculator() {
	parseProc();
	for( auto &x: m_stat ) {
		// чтобы дельта была сначала нулевая
		x.second.old = x.second.current;
	}
}


bool IfaceStatCalculator::parseProc() {
	ifstream ifs("/proc/net/dev");

	if( !ifs.is_open() ) {
		throw runtime_error("can't read /proc/net/dev");
	}

	string line;
	getline(ifs, line);
	getline(ifs, line); // две строки заголовка

	while( getline(ifs, line) ) {

		// делим строку на имя интерфейса и все остальное
		auto tokens = getTokens(line, [](char c) {
			return c == ':';
		});

		// at вызовет исключение, мы хотябы узнаем о беде... в main
		string iface( ltrimCopy(tokens.at(0)) );

		// а теперь столбцы числел разделенные вайтспейсом ( 16 штук )
		tokens = getTokens( tokens.at(1) );

		Deltas& deltas = m_stat[iface];
		deltas.old = deltas.current;
		deltas.current.m_rx = atoll( tokens.at(0).c_str() );
		deltas.current.m_tx = atoll( tokens.at(8).c_str() );
	}

	return true;
}


void IfaceStatCalculator::dumpPacket ( ostream& stream ) const {
	stream << "ifaces:";
	for( auto &x: m_stat ) {
		stream << ' ' << x.first 
			<< ' ' << x.second.current.m_rx - x.second.old.m_rx
			<< ' ' << x.second.current.m_tx - x.second.old.m_tx;
	}
	stream << '\n';
}


bool IfacesStatList::parseString ( const string& s ) {
	string iface;
	unsigned rx_speed, tx_speed;
	istringstream is(s);

	while( (is >> iface >> rx_speed >> tx_speed) ) {
		auto &x = m_stat[iface];
		x.m_rx = rx_speed;
		x.m_tx = tx_speed;
	}

	return true;
}

